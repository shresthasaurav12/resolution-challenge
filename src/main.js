import Vue from 'vue'
import App from './App.vue'
import { makeServer } from "./server"
import vuetify from '@/plugins/vuetify' // path to vuetify export
Vue.config.productionTip = false

// only if dev env is in developement
// if (process.env.NODE_ENV === "development") {
makeServer()
// }


new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
