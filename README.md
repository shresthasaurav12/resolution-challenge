# Resolution-Challenge

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Project Deployed on 
https://amazing-turing-59b33e.netlify.app/ 

### Project Approach
Project was completed before 6hr but instead of continous development each requirement was developed in various time slots.  

- For the project, only pagination component was created and other requirements were managed with different methods.
- Vuetify is used for simple table and pagination buttons
- Mirage JS is used to mock the API for given data. 
- Axios used to get the return promise. 

### Code current
- Multiple selection filter option is added for type and post code. 
- Single pagination components.
- Search, Filter , Sort logic implmented.
- Loadash used for debouncing search query.


### Code ehnahcement
- If the code was for real time, the approach would have been to develop complete reusable components base structure For datatable.
    - e.g. BaseTable, Filter, Search reusable components. 
- Implemnted Vuex for state management.
- Search, and Filter functionality could be further enhanced. 

### Testing
- Basic level of functionality testing and debugging is completed. 
    For more Testing Jest would have been used to test the components structure and props. 

